define ['Admin/Main/Ctrl/Base'], (Admin_Ctrl_Base) ->
	class Admin_Main_Ctrl_Bare extends Admin_Ctrl_Base
		@CTRL_ID   = 'Admin_Main_Ctrl_Bare'

	Admin_Main_Ctrl_Bare.EXPORT_CTRL()