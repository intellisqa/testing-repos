(function() {
  define(['angular'], function(angular) {
    var ReportsModule;
    ReportsModule = angular.module('Reports_App', ['ngAnimate', 'ngSanitize', 'ui.router', 'ui.bootstrap', 'ui.select2', 'ui.sortable', 'angularMoment', 'blueimp.fileupload', 'uiSlider', 'deskpro.option_builder', 'deskpro.category_builder']);
    ReportsModule.config([
      'datepickerConfig', 'datepickerPopupConfig', function(datepickerConfig, datepickerPopupConfig) {
        datepickerConfig.showWeeks = false;
        datepickerPopupConfig.showWeeks = false;
        return datepickerPopupConfig.dateFormat = 'dd MMMM yyyy';
      }
    ]);
    return ReportsModule;
  });

}).call(this);

//# sourceMappingURL=ReportsModule.js.map
