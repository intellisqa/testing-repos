define [
	'DeskPRO/Main/Ctrl/Base'
], (
	DeskPROBaseCtrl
) ->
	class Reports_Main_Ctrl_Bare extends DeskPROBaseCtrl
		@CTRL_ID = 'Reports_Main_Ctrl_Bare'

	Reports_Main_Ctrl_Bare.EXPORT_CTRL()