<?php

return array(
	'id' => 1,
	'gravatar_url' => 'http://www.gravatar.com/avatar/e64c7d89f26bd1972efa854d13d7dd61?&d=mm',
	'disable_picture' => false,
	'is_contact' => true,
	'is_user' => true,
	'is_agent' => true,
	'was_agent' => false,
	'can_agent' => true,
	'can_admin' => true,
	'can_billing' => true,
	'can_reports' => true,
	'is_vacation_mode' => false,
	'disable_autoresponses' => false,
	'disable_autoresponses_log' => '',
	'is_confirmed' => true,
	'is_agent_confirmed' => true,
	'is_deleted' => false,
	'is_disabled' => false,
	'importance' => 0,
	'creation_system' => 'web.person',
	'name' => 'Admin Admin',
	'first_name' => 'Admin',
	'last_name' => 'Admin',
	'title_prefix' => '',
	'override_display_name' => '',
	'summary' => '',
	'organization_position' => '',
	'organization_manager' => false,
	'timezone' => 'UTC',
	'date_created' => '2014-04-22 08:12:01',
	'date_created_ts' => 1398154321,
	'date_created_ts_ms' => 1398154321000,
	'date_last_login' => NULL,
	'date_last_login_ts' => 0,
	'date_last_login_ts_ms' => 0,
	'date_picture_check' => NULL,
	'date_picture_check_ts' => 0,
	'date_picture_check_ts_ms' => 0,
	'organization' => NULL,
	'emails' =>
	array(
		0 =>
		array(
			'id' => 1,
			'email' => 'admin@example.com',
		),
	),
	'custom_data' =>
	array(
	),
	'contact_data' =>
	array(
	),
	'usergroups' =>
	array(
		0 =>
		array(
			'id' => 3,
			'title' => 'All Permissions',
			'note' => '',
			'is_agent_group' => true,
			'sys_name' => NULL,
			'is_enabled' => true,
		),
	),
	'labels' =>
	array(
	),
	'display_name' => 'Admin Admin',
	'primary_email' =>
	array(
		'id' => 1,
		'email' => 'admin@example.com',
	),
	'usergroup_ids' =>
	array(
	),
	'agentgroup_ids' =>
	array(
		0 => 3,
	),
	'picture_url' => 'http://localhost:8888/file.php/avatar/80/default.jpg?size-fit=1',
	'picture_url_80' => 'http://localhost:8888/file.php/avatar/80/default.jpg?size-fit=1',
	'picture_url_64' => 'http://localhost:8888/file.php/avatar/64/default.jpg?size-fit=1',
	'picture_url_50' => 'http://localhost:8888/file.php/avatar/50/default.jpg?size-fit=1',
	'picture_url_45' => 'http://localhost:8888/file.php/avatar/45/default.jpg?size-fit=1',
	'picture_url_32' => 'http://localhost:8888/file.php/avatar/32/default.jpg?size-fit=1',
	'picture_url_22' => 'http://localhost:8888/file.php/avatar/22/default.jpg?size-fit=1',
	'picture_url_16' => 'http://localhost:8888/file.php/avatar/16/default.jpg?size-fit=1',
);
