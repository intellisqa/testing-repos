<?php return array(
	'agent.settings.notif_contact_admin'                                   => 'Пожалуйста, свяжитесь с администратором,, чтобы обновить настройки уведомлений.',
	'agent.settings.notif_group_follow'                                    => 'Мои подписки',
	'agent.settings.notif_group_unassigned'                                => 'Не назначенные тикеты',
	'agent.settings.notif_take_due_today'                                  => 'Назначенные на меня задачи, истекающие сегодня',
	'agent.settings.notif_task_assigned'                                   => 'Задачи, назначенные на меня',
	'agent.settings.notif_task_assigned_team'                              => 'Задачи, назначенные на мою команду',
	'agent.settings.notif_task_delegated_complete'                         => 'Переданные мною и завершенные задачи',
	'agent.settings.notif_tweet_assigned'                                  => 'Твиты, назначенные на меня',
	'agent.settings.notif_tweet_assigned_team'                             => 'Твиты, назначенные на мою команду',
	'agent.settings.notif_type_assign'                                     => 'Назначение',
	'agent.settings.sla_filter_assigned'                                   => 'Создано',
	'agent.settings.sla_filter_team'                                       => 'Показать только тикеты, назначенные на мою команду',
);
