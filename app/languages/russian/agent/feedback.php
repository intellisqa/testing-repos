<?php return array(
	'agent.feedback.any_category'                                          => 'Любая категория',
	'agent.feedback.any_status'                                            => 'Любой статус',
	'agent.feedback.count_votes'                                           => '{{display_count}} голос|{{display_count}} голосов',
	'agent.feedback.feedback_deleted'                                      => 'Предложение удалено',
	'agent.feedback.feedback_to_validate'                                  => 'Предложение для подтверждения',
	'agent.feedback.list_title'                                            => '1 зависимость|{{count}} зависимостей',
	'agent.feedback.merge_feedback'                                        => 'Объединить предложения',
	'agent.feedback.merge_feedback_note'                                   => 'Описание предложения при объединении будет добавлено к сохраненному предложению.',
	'agent.feedback.merge_feedback_submit'                                 => 'Объединить эти зависимые предложения',
);
