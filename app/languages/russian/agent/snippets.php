<?php return array(
	'agent.snippets.add_snippet'                                           => 'Добавить фрагмент',
	'agent.snippets.all_snippets'                                          => 'Все фрагменты',
	'agent.snippets.cat_can_be_used_by'                                    => 'Категория использовалась:',
	'agent.snippets.chat_snippets'                                         => 'Фрагменты чата',
	'agent.snippets.edit_snippet'                                          => 'Редактировать фрагмент',
	'agent.snippets.ticket_snippets'                                       => 'Фрагменты тикета',
);
