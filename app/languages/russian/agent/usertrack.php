<?php return array(
	'agent.usertrack.first_visit_date'                                     => 'Дата первого входа',
	'agent.usertrack.initial_landing_page'                                 => 'Первоначальная целевая страница',
	'agent.usertrack.initial_visit_time'                                   => 'Первоначальное время визита',
	'agent.usertrack.landing_pages'                                        => 'Целевые страницы',
	'agent.usertrack.number_of_pages'                                      => 'Номер страницы',
	'agent.usertrack.number_of_visits'                                     => 'Число посещений',
	'agent.usertrack.session_landing_page'                                 => 'Сессионная целевая страница',
	'agent.usertrack.session_start_time'                                   => 'Время начала сессии',
	'agent.usertrack.visited_pages'                                        => 'Посещенные страницы',
	'agent.usertrack.visitor_id'                                           => 'ID посетителя',
);
