<?php return array(
	'agent.userchat.agent_blocked_user_on'                                 => '{{name}} a bloqué le {{date}}',
	'agent.userchat.assigned_to'                                           => 'Chat assigné à {{name}}',
	'agent.userchat.block_ip_address'                                      => 'Bloquer l\'adresse IP',
	'agent.userchat.block_reason'                                          => 'Raison: {{reason}}',
	'agent.userchat.block_title'                                           => 'Terminer le chat et empêcher cet utilisateur de commencer un nouveau chat dans le prochaines 24 heures.',
	'agent.userchat.block_user'                                            => 'Bloquer un utilisateur',
	'agent.userchat.create_ticket'                                         => 'Créer un ticket',
	'agent.userchat.ended_by'                                              => 'Chat terminé par {{name}}',
	'agent.userchat.ended_by_reason'                                       => 'Chat terminé par {{name}}: {{reason}}',
	'agent.userchat.ended_user'                                            => 'Chat terminé par l\'utilisateur',
	'agent.userchat.full_message'                                          => 'Message complet',
	'agent.userchat.ident_unconfirmed'                                     => 'Identité non confirmée, l\'email n\'a pas été validé',
	'agent.userchat.list_title'                                            => '1 chat|{{count}} chats',
	'agent.userchat.message_agent-timeout'                                 => '{{name}} ne répond pas. Veuillez patienter pendant que nous trouvons un autre agent.',
	'agent.userchat.message_assigned'                                      => 'Chat assigné à {{name}}',
	'agent.userchat.message_ended'                                         => 'Chat terminé',
	'agent.userchat.message_ended-by'                                      => 'Chat terminé par {{name}}',
	'agent.userchat.message_ended-by-user'                                 => 'Chat terminé par l\'utilisateur',
	'agent.userchat.message_set-department'                                => '{{name}} définit le département à {{department}}',
	'agent.userchat.message_set_department'                                => '{{name}} définit le département à {{department}}',
	'agent.userchat.message_started'                                       => 'Chat commencé',
	'agent.userchat.message_unassigned'                                    => 'Chat non assigné',
	'agent.userchat.message_uploading'                                     => 'En cours de téléchargement...',
	'agent.userchat.message_user-joined'                                   => '{{name}} a rejoint le chat',
	'agent.userchat.message_user-left'                                     => '{{name}} a quitté le chat',
	'agent.userchat.message_user-returned'                                 => 'L\'utilisateur est revenu',
	'agent.userchat.message_user-timeout'                                  => 'Utilisateur déconnecté',
	'agent.userchat.message_user_joined'                                   => '{{name}} a rejoint le chat',
	'agent.userchat.message_user_left'                                     => '{{name}} a quitté le chat',
	'agent.userchat.message_wait-timeout'                                  => 'Chat terminé: aucun agent n\'a pu être trouvé',
	'agent.userchat.msg_agent_timeout'                                     => '{{name}} ne répond pas. Veuillez patienter pendant que nous trouvons un autre agent.',
	'agent.userchat.msg_new_user_track'                                    => 'L\'utilisateur regarde : {{label}}',
	'agent.userchat.msg_user_timeout'                                      => 'L\'utilisateur ne répond pas.',
	'agent.userchat.new_chat'                                              => 'Nouveau chat',
	'agent.userchat.take_chat'                                             => 'Prendre le chat',
	'agent.userchat.taken_by'                                              => 'Chat pris par',
	'agent.userchat.transcript_sent'                                       => 'Transcription envoyée à {{email}}',
	'agent.userchat.unassigned'                                            => 'Chat non assigné',
	'agent.userchat.unblock'                                               => 'Débloquer',
	'agent.userchat.user_is_blocked'                                       => 'L\'utilisateur est bloqué',
	'agent.userchat.visitor_info'                                          => 'Information visiteur',
	'agent.userchat.you_have_been_invited'                                 => 'Vous avez été invitée à rejoindre le chat',
);
