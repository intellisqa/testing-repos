<?php return array(
	'user.news.all-categories'                                             => '모든 카테고리',
	'user.news.category'                                                   => '카테고리',
	'user.news.latest_news'                                                => '최신 뉴스',
	'user.news.news_not_found'                                             => '게시물을 찾을 수 없습니다.',
	'user.news.posted_date'                                                => '게시 날짜: {{date}}',
	'user.news.related_news'                                               => '관련 뉴스',
	'user.news.title'                                                      => '뉴스',
	'user.news.type_news'                                                  => '뉴스',
);
