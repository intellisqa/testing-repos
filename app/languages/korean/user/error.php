<?php return array(
	'user.error.attach_ext-allowed'                                        => '{{error_detail}} 파일 종류만 업로드 하실 수 있습니다.',
	'user.error.attach_ext-not-allow'                                      => '업로드 하신 파일은 허용되지 않습니다.',
	'user.error.attach_failed'                                             => '첨부 파일이 업로드에 실패했습니다 (최소하셨습니까?). 다시 시도해 주십시오.',
	'user.error.attach_no-file'                                            => '업로드 하실 파일을 선택해 주세요.',
	'user.error.attach_size'                                               => '첨부하신 파일이 너무 큽니다. 최대 사이즈는 {{error_detail}}',
	'user.error.attach_unknown-error'                                      => '첨부된 파일을 업로드 하는 도중에 서버에 문제가 생겼습니다. 다시 한번 시도해 주세요.',
	'user.error.error_code'                                                => '오류 코드',
	'user.error.error_title'                                               => '오류',
	'user.error.expired-token'                                             => '페이지가 만료되었습니다. 뒤로가기를 누르시고 다시 시도해 주세요.',
	'user.error.form_choice_invalid'                                       => '리스트에서 옵션을 선택해 주세요.',
	'user.error.form_choice_max'                                           => '한개의 옵션만 선택해 주세요 최대 {{count}} 개의 옵션만 선택해 주세요. ',
	'user.error.form_choice_min'                                           => '옵션을 1개 이상 선택해 주세요. {{count}} 개 이상의 옵션을 선택해 주세요.',
	'user.error.form_date_invalid'                                         => '정확한 날짜를 입력해 주세요',
	'user.error.form_date_invalid_dow'                                     => '선택하신 날은 가능하지 않습니다',
	'user.error.form_date_invalid_range'                                   => '선택하신 날짜는 가능하지 않습니다.',
	'user.error.form_required'                                             => '이 칸은 반드시 작성해 주셔야 합니다.',
	'user.error.form_text_max'                                             => '최대 1개의 글자를 입력 해 주세요. 최대 {{count}} 개의 글자를 입력 해 주세요.',
	'user.error.form_text_min'                                             => '최소 1개의 글자를 입력해 주세요. 최소 {{count}} 개의 글자를 입력해주세요.',
	'user.error.form_text_regex'                                           => '정확한 가격을 입력해 주세요.',
	'user.error.invalid_email'                                             => '맞지 않는 이메일 주소 입니다.',
	'user.error.invalid_email-code'                                        => '이 링크는 맞지 않습니다.',
	'user.error.invalid_email-explain'                                     => '계정과 일치하는 이메일 주소를 찾지 못했습니다.',
	'user.error.not-found'                                                 => '페이지를 찾을 수 없습니다.',
	'user.error.not-found-title'                                           => '찾을 수 없습니다.',
	'user.error.permission-denied'                                         => '죄송합니다. 요청하신 작업은 허가되지 않았습니다',
	'user.error.permission-denied-login'                                   => '로그아웃 되셨습니다. <a href="{{login_url}}"> 로그인하시고 </a> 다시 시도 해 주십시오.',
	'user.error.permission-denied-title'                                   => '요청하신 작업에 대한 권한이 없습니다.',
	'user.error.server-error'                                              => '페이지를 로딩 하는데 서버 오류가 있었습니다. 다시 시도 해주십시오.',
);
