<?php

return array(
	'agent.jira.error'				=> 'Error occured',
	'agent.jira.export_jira'		=> 'Export to JIRA',
	'agent.jira.post_comment'		=> 'Add a comment',
	'agent.jira.unlink'				=> 'Unlink JIRA issue',
	
	// Form labels
	'agent.jira.labels.title'		=> 'Issue Title',
	'agent.jira.labels.project'		=> 'Project',
	'agent.jira.labels.assignee'	=> 'Assign To',
	'agent.jira.labels.type'		=> 'Type',
	'agent.jira.labels.priority'	=> 'Priority',
	'agent.jira.labels.priority'	=> 'Priority',
	'agent.jira.labels.description'	=> 'Description',
	'agent.jira.labels.labels'		=> 'Labels',
	'agent.jira.labels.reset'		=> 'Reset to defaults',
	'agent.jira.labels.export'		=> 'Export',
	'agent.jira.labels.comment'		=> 'Comment',
	'agent.jira.labels.unlink'		=> 'Unlink',
	
	'agent.jira.labels.smarttags'	=> 'You can also use the following "smart tags" to add DeskPRO relevant information to this issue.',
	
	'agent.jira.labels.description.warning'	=> 'Please be sure to check the contents for any confidential information before exporting',
	'agent.jira.labels.labels.instrcution'	=> 'Add some tags/labels to better classify this issue',
		
	// Form default values/labels
	'agent.jira.select.project'		=> 'Select Project',
	'agent.jira.nobody'				=> 'Nobody',
	
	// Issue table headers
	'agent.jira.issuekey'			=> 'Issue Key',
	'agent.jira.status'				=> 'Status',
	'agent.jira.assignedto'			=> 'Assigned To',
	'agent.jira.lastactivity'		=> 'Last Activity',
	'agent.jira.actions'			=> 'Actions',
	
	'agent.jira.optional'			=> 'optional'
);