<?php return array(
	'adm.twitter_accounts.new_twitter_account'            => 'New Twitter Account',
	'adm.twitter_accounts.twitter_account'                => 'Twitter Account',
	'adm.twitter_accounts.twitter_access'                 => 'Twitter Access',
	'adm.twitter_accounts.twitter_access_explain'         => 'Account is verified when you create new account.',
	'adm.twitter_accounts.agents_explain'                 => 'These are agents allowed to access twitter application.',
	'adm.twitter_accounts.delete_twitter_account_confirm' => 'Are you sure you want to delete this twitter account? This action cannot be undone.',
);