<?php return array(
	'adm.ticket_triggers.triggers'       => 'Triggers',
	'adm.ticket_triggers.new_trigger'    => 'New Trigger',
	'adm.ticket_triggers.delete_confirm' => 'Are you sure you want to delete this trigger? Any actions this trigger used to perform will no longer work.',
	'adm.ticket_triggers.title_explain'  => 'This title will be used throughout the admin interface to refer to this trigger.',
	'adm.ticket_triggers.trigger_newticket_email' => 'Email the user an automated message about their new ticket',
	'adm.ticket_triggers.no_triggers'    => 'You have not created any triggers here yet.',
	'adm.ticket_triggers.count_triggers' => '{{count}} Trigger|{{count}} Triggers',
	'adm.ticket_triggers.count_dep_triggers' => '{{count}} Department Trigger|{{count}} Department Triggers',
	'adm.ticket_triggers.count_email_triggers' => '{{count}} Email Account Trigger|{{count}} Email Account Triggers',
);