<?php return array(
	'adm.ticket_filters.filters'        => 'Filters',
	'adm.ticket_filters.new_filter'     => 'New Filter',
	'adm.ticket_filters.delete_confirm' => 'Are you sure you want to delete this filter?',
	'adm.ticket_filters.title_explain'  => 'This title will be displayed in the agent interface in the Filter list. It should be kept short.',
	'adm.ticket_filters.no_filters'     => 'You have not created any filters yet.',
	'adm.ticket_filters.count_filters'  => '{{count}} Filter|{{count}} Filters',
);