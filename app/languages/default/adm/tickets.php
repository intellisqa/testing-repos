<?php return array(
	'adm.tickets.account_details'         => 'Account Details',
	'adm.tickets.delete_account'          => 'Delete Ticket Account',
	'adm.tickets.delete_account_confirm'  => 'Are you sure you want to delete this email account? DeskPRO will stop processing emails from this account.',
	'adm.tickets.email_accounts'          => 'Email Accounts',
	'adm.tickets.new_account'             => 'New Account',
	'adm.tickets.no_email_accounts'       => 'You have not created any email accounts yet.',
	'adm.tickets.x_email_accounts'        => '{{count}} Email Account|{{count}} Email Accounts',
);