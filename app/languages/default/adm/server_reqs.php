<?php return array(
	'adm.server_reqs.check_server_reqs' => 'Check Server Requirements',
	'adm.server_reqs.web_checks'        => 'Web Checks',
	'adm.server_reqs.cli_checks'        => 'Command-line Checks',
	'adm.server_reqs.read_more_about'   => 'Read more about fixing this error',
);