<?php return array(
	'adm.feedback_statuses.no_delete_last'                 => 'At least one feedback status must always exist. You cannot delete the last feedback status.',
	'adm.feedback_statuses.title_explain'                  => 'This is the title as it will appear throughout the agent and user interfaces.',
	'adm.feedback_statuses.title_error'                    => 'You must enter a valid title',
	'adm.feedback_statuses.title'                          => 'Title',
	'adm.feedback_statuses.delete_feedback_status'         => 'Delete Feedback Status',
	'adm.feedback_statuses.delete_feedback_status_confirm' => 'Are you sure you want to delete this feedback status?',
	'adm.feedback_statuses.delete_feedback_statuses_moved' => 'Any feedback that is currently under this feedback status will be moved to:',
);