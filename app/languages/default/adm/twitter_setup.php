<?php return array(
	'adm.twitter_setup.title'                          => 'Twitter Setup',
	'adm.twitter_setup.agent_consumer_key'             => 'Consumer Key',
	'adm.twitter_setup.agent_consumer_key_error'       => 'You must enter Consumer Key for Twitter Agent Application',
	'adm.twitter_setup.agent_consumer_secret'          => 'Consumer Secret',
	'adm.twitter_setup.agent_consumer_secret_error'    => 'You must enter Consumer Secret for Twitter Agent Application',
	'adm.twitter_setup.user_consumer_key'              => 'Consumer Key',
	'adm.twitter_setup.user_consumer_key_error'        => 'You must enter Consumer Key for Twitter User Application',
	'adm.twitter_setup.user_consumer_secret'           => 'Consumer Secret',
	'adm.twitter_setup.user_consumer_secret_error'     => 'You must enter Consumer Secret for Twitter User Application',
	'adm.ticket_filters.agent_consumer_key_explain'    => 'This is consumer key for Twitter Agent Application',
	'adm.ticket_filters.agent_consumer_secret_explain' => 'This is consumer secret for Twitter Agent Application',
	'adm.ticket_filters.user_consumer_key_explain'     => 'This is consumer key for Twitter User Application',
	'adm.ticket_filters.user_consumer_secret_explain'  => 'This is consumer secret for Twitter User Application',
	'adm.ticket_filters.auto_remove_time'              => 'How long until Tweets that aren\'t directed specifically to your accounts are removed?',
	'adm.ticket_filters.auto_remove_time_explain'      => 'General timeline, search, and user profile tweets will be removed with this. Any tweets that appear under "All Tweets" will not be automatically removed.',
);