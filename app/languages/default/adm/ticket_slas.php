<?php return array(
	'adm.ticket_slas.slas'           => 'SLAs',
	'adm.ticket_slas.delete_confirm' => 'Are you sure you want to delete this SLA? All data associated with the SLA (such as reports based on this SLA) will be deleted too.',
	'adm.ticket_slas.title_explain'  => 'This title will be displayed in the agent interface when listing SLAs.',
	'adm.ticket_slas.no_slas'        => 'You have not created any SLAs yet.',
	'adm.ticket_slas.count_slas'     => '{{count}} SLA|{{count}} SLAs',
);