<?php return array(
	'user.lang.lang_title'                                                 => 'العربية',
	'user.lang.lang_title_arabic'                                          => 'العربية',
	'user.lang.lang_title_default'                                         => 'الإنجليزية',
	'user.lang.lang_title_french'                                          => 'الفرنسية',
	'user.lang.lang_title_italian'                                         => 'الإيطالية',
	'user.lang.lang_title_japanese'                                        => 'اليابانية',
	'user.lang.lang_title_persian'                                         => 'الفارسية',
	'user.lang.lang_title_romanian'                                        => 'الرومانية',
	'user.lang.lang_title_russian'                                         => 'الروسية',
	'user.lang.lang_title_spanish'                                         => 'الإسبانية',
	'user.lang.lang_title_swedish'                                         => 'السويدية',
	'user.lang.lang_title_turkish'                                         => 'التركية',
);
