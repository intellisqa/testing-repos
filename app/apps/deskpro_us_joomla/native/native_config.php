<?php return array(
	'install' => array(
		'handler' => 'deskpro_us_joomla\\InstallerHandler'
	),
	'api' => array(
		'package_request_handler' => 'deskpro_us_joomla\\RequestHandler\\PackageRequestHandler'
	)
);