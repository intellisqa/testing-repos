<?php return array(
	'install' => array(
		'handler' => 'deskpro_us_google\\InstallerHandler'
	),
	'api' => array(
		'package_request_handler' => 'deskpro_us_google\\RequestHandler\\PackageRequestHandler'
	)
);