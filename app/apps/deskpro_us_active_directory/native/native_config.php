<?php return array(
	'install' => array(
		'handler' => 'deskpro_us_active_directory\\InstallerHandler'
	),
	'api' => array(
		'package_request_handler' => 'deskpro_us_active_directory\\RequestHandler\\PackageRequestHandler'
	)
);