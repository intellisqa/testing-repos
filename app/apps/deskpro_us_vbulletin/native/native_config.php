<?php return array(
	'install' => array(
		'handler' => 'deskpro_us_vbulletin\\InstallerHandler'
	),
	'api' => array(
		'package_request_handler' => 'deskpro_us_vbulletin\\RequestHandler\\PackageRequestHandler'
	)
);