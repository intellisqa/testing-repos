<?php return array(
	'install' => array(
		'handler' => 'deskpro_us_xenforo\\InstallerHandler'
	),
	'api' => array(
		'package_request_handler' => 'deskpro_us_xenforo\\RequestHandler\\PackageRequestHandler'
	)
);