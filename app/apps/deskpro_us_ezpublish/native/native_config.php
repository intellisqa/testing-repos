<?php return array(
	'install' => array(
		'handler' => 'deskpro_us_ezpublish\\InstallerHandler'
	),
	'api' => array(
		'package_request_handler' => 'deskpro_us_ezpublish\\RequestHandler\\PackageRequestHandler'
	)
);