<?php return array(
	'install' => array(
		'handler' => 'deskpro_magento\\InstallerHandler'
	),
	'api' => array(
		'package_request_handler' => 'deskpro_magento\\RequestHandler\\PackageRequestHandler'
	)
);