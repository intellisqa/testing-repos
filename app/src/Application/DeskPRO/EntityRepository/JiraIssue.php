<?php

namespace Application\DeskPRO\EntityRepository;

use Application\DeskPRO\App;

/**
 * JIRA issues entity repository
 * 
 * @author Abhinav Kumar <work@abhinavkumar.in>
 */
class JiraIssue extends AbstractEntityRepository
{
}