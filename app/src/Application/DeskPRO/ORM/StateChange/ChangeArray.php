<?php
/**************************************************************************\
| DeskPRO (r) has been developed by DeskPRO Ltd. http://www.deskpro.com/   |
| a British company located in London, England.                            |
|                                                                          |
| All source code and content Copyright (c) 2012, DeskPRO Ltd.             |
|                                                                          |
| The license agreement under which this software is released              |
| can be found at http://www.deskpro.com/license                           |
|                                                                          |
| By using this software, you acknowledge having read the license          |
| and agree to be bound thereby.                                           |
|                                                                          |
| Please note that DeskPRO is not free software. We release the full       |
| source code for our software because we trust our users to pay us for    |
| the huge investment in time and energy that has gone into both creating  |
| this software and supporting our customers. By providing the source code |
| we preserve our customers' ability to modify, audit and learn from our   |
| work. We have been developing DeskPRO since 2001, please help us make it |
| another decade.                                                          |
|                                                                          |
| Like the work you see? Think you could make it better? We are always     |
| looking for great developers to join us: http://www.deskpro.com/jobs/    |
|                                                                          |
| ~ Thanks, Everyone at Team DeskPRO                                       |
\**************************************************************************/

/**
 * DeskPRO
 *
 * @package DeskPRO
 * @category Entities
 */

namespace Application\DeskPRO\ORM\StateChange;

use Orb\Util\Arrays;

class ChangeArray implements ChangeInterface
{
	/**
	 * @var string
	 */
	private $field_id;

	/**
	 * @var array
	 */
	private $old;

	/**
	 * @var array
	 */
	private $new;

	/**
	 * @var bool
	 */
	private $is_same = false;


	/**
	 * @param string $field_id
	 * @param mixed  $old
	 * @param mixed  $new
	 */
	public function __construct($field_id, array $old = null, array $new = null)
	{
		$this->field_id = $field_id;
		$this->old      = $old;
		$this->new      = $new;

		// Check for null
		if ($old === $new) {
			$this->is_same = true;
		} elseif ($old && $new && count($this->old) == count($this->new)) {
			if (Arrays::arrayDiffAssocRecursive($this->old, $this->new) || Arrays::arrayDiffAssocRecursive($this->new, $this->old)) {
				$this->is_same = true;
			}
		}
	}


	/**
	 * @return string
	 */
	public function getField()
	{
		return $this->field_id;
	}


	/**
	 * @return array
	 */
	public function getOld()
	{
		return $this->old;
	}


	/**
	 * @return array
	 */
	public function getNew()
	{
		return $this->new;
	}


	/**
	 * @return bool
	 */
	public function isSame()
	{
		return $this->is_same;
	}


	/**
	 * @return bool
	 */
	public function isCollection()
	{
		return false;
	}


	/**
	 * @return bool
	 */
	public function isEntity()
	{
		return false;
	}
}