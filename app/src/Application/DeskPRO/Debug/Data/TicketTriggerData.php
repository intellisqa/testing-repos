<?php
/**************************************************************************\
| DeskPRO (r) has been developed by DeskPRO Ltd. http://www.deskpro.com/   |
| a British company located in London, England.                            |
|                                                                          |
| All source code and content Copyright (c) 2012, DeskPRO Ltd.             |
|                                                                          |
| The license agreement under which this software is released              |
| can be found at http://www.deskpro.com/license                           |
|                                                                          |
| By using this software, you acknowledge having read the license          |
| and agree to be bound thereby.                                           |
|                                                                          |
| Please note that DeskPRO is not free software. We release the full       |
| source code for our software because we trust our users to pay us for    |
| the huge investment in time and energy that has gone into both creating  |
| this software and supporting our customers. By providing the source code |
| we preserve our customers' ability to modify, audit and learn from our   |
| work. We have been developing DeskPRO since 2001, please help us make it |
| another decade.                                                          |
|                                                                          |
| Like the work you see? Think you could make it better? We are always     |
| looking for great developers to join us: http://www.deskpro.com/jobs/    |
|                                                                          |
| ~ Thanks, Everyone at Team DeskPRO                                       |
\**************************************************************************/

/**
 * DeskPRO
 *
 * @package DeskPRO
 */

namespace Application\DeskPRO\Debug\Data;

use Application\DeskPRO\App;

class TicketTriggerData implements DataInterface
{
	public function getData()
	{
		$ret = array();

		$data = App::getDb()->fetchAll("SELECT * FROM ticket_triggers ORDER BY id ASC");
		foreach ($data as &$d) {
			if (!empty($d['terms'])) {
				$d['terms'] = @json_decode($d['terms'], true);
			}
			if (!empty($d['actions'])) {
				$d['actions'] = @json_decode($d['actions'], true);
			}
		}
		unset($d);
		$ret['triggers'] = $data;

		$data = App::getDb()->fetchAll("SELECT * FROM slas ORDER BY id ASC");
		$ret['slas'] = $data;

		return $ret;
	}
}