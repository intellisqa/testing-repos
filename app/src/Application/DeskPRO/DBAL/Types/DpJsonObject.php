<?php
/**************************************************************************\
| DeskPRO (r) has been developed by DeskPRO Ltd. http://www.deskpro.com/   |
| a British company located in London, England.                            |
|                                                                          |
| All source code and content Copyright (c) 2012, DeskPRO Ltd.             |
|                                                                          |
| The license agreement under which this software is released              |
| can be found at http://www.deskpro.com/license                           |
|                                                                          |
| By using this software, you acknowledge having read the license          |
| and agree to be bound thereby.                                           |
|                                                                          |
| Please note that DeskPRO is not free software. We release the full       |
| source code for our software because we trust our users to pay us for    |
| the huge investment in time and energy that has gone into both creating  |
| this software and supporting our customers. By providing the source code |
| we preserve our customers' ability to modify, audit and learn from our   |
| work. We have been developing DeskPRO since 2001, please help us make it |
| another decade.                                                          |
|                                                                          |
| Like the work you see? Think you could make it better? We are always     |
| looking for great developers to join us: http://www.deskpro.com/jobs/    |
|                                                                          |
| ~ Thanks, Everyone at Team DeskPRO                                       |
\**************************************************************************/

/**
 * DeskPRO
 *
 * @package DeskPRO
 * @category Entities
 */

namespace Application\DeskPRO\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Orb\Types\JsonObjectSerializable;
use Orb\Types\JsonObjectSerializer;

class DpJsonObject extends Type
{
	const DP_JSON_OBJ = 'dp_json_obj';


	/**
	 * {@inheritdoc}
	 */
	public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
	{
		return $platform->getClobTypeDeclarationSQL($fieldDeclaration);
	}


	/**
	 * {@inheritdoc}
	 */
	public function convertToDatabaseValue($value, AbstractPlatform $platform)
	{
		if (null === $value) {
			return null;
		}

		if (!is_object($value) || !($value instanceof JsonObjectSerializable)) {
			throw new \InvalidArgumentException("Class is not JsonObjectSerializable");
		}

		return JsonObjectSerializer::serialize($value);
	}


	/**
	 * {@inheritdoc}
	 */
	public function convertToPHPValue($value, AbstractPlatform $platform)
	{
		if ($value === null) {
			return null;
		}

		$value = (is_resource($value)) ? stream_get_contents($value) : $value;

		return JsonObjectSerializer::unserialize($value);
	}

	/**
	 * {@inheritdoc}
	 */
	public function getName()
	{
		return self::DP_JSON_OBJ;
	}

	/**
	 * {@inheritdoc}
	 */
	public function requiresSQLCommentHint(AbstractPlatform $platform)
	{
		return true;
	}
}
