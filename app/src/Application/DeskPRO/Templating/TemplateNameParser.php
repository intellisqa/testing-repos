<?php
/**************************************************************************\
| DeskPRO (r) has been developed by DeskPRO Ltd. http://www.deskpro.com/   |
| a British company located in London, England.                            |
|                                                                          |
| All source code and content Copyright (c) 2012, DeskPRO Ltd.             |
|                                                                          |
| The license agreement under which this software is released              |
| can be found at http://www.deskpro.com/license                           |
|                                                                          |
| By using this software, you acknowledge having read the license          |
| and agree to be bound thereby.                                           |
|                                                                          |
| Please note that DeskPRO is not free software. We release the full       |
| source code for our software because we trust our users to pay us for    |
| the huge investment in time and energy that has gone into both creating  |
| this software and supporting our customers. By providing the source code |
| we preserve our customers' ability to modify, audit and learn from our   |
| work. We have been developing DeskPRO since 2001, please help us make it |
| another decade.                                                          |
|                                                                          |
| Like the work you see? Think you could make it better? We are always     |
| looking for great developers to join us: http://www.deskpro.com/jobs/    |
|                                                                          |
| ~ Thanks, Everyone at Team DeskPRO                                       |
\**************************************************************************/

/**
 * DeskPRO
 *
 * @package DeskPRO
 * @subpackage Templating
 */

namespace Application\DeskPRO\Templating;

use Symfony\Bundle\FrameworkBundle\Templating\TemplateNameParser as BaseTemplateNameParser;
use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference as BundleTemplateReference;
use Symfony\Component\Templating\TemplateReferenceInterface;

class TemplateNameParser extends BaseTemplateNameParser
{
	/**
	 * {@inheritdoc}
	 */
	public function parse($name)
	{
		if ($name instanceof TemplateReferenceInterface) {
			return $name;
		} elseif (isset($this->cache[$name])) {
			return $this->cache[$name];
		}

		// normalize name
		$name = str_replace(':/', ':', preg_replace('#/{2,}#', '/', strtr($name, '\\', '/')));

		if (false !== strpos($name, '..')) {
			throw new \RuntimeException(sprintf('Template name "%s" contains invalid characters.', $name));
		}

		if (!preg_match('/^([^:]*):([^:]*):(.+)\.([^\.]+)\.([^\.]+)$/', $name, $matches)) {
			throw new \InvalidArgumentException(sprintf('Template name "%s" is not valid (format is "bundle:section:template.format.engine").', $name));
		}

		if ($matches[1] == 'Apps') {
			$template = new BundleTemplateReference(null, $matches[2], $matches[3], $matches[4], $matches[5]);
		} else {
			$template = new BundleTemplateReference($matches[1], $matches[2], $matches[3], $matches[4], $matches[5]);
		}

		if ($template->get('bundle')) {
			try {
				$this->kernel->getBundle($template->get('bundle'));
			} catch (\Exception $e) {
				throw new \InvalidArgumentException(sprintf('Template name "%s" is not valid.', $name), 0, $e);
			}
		}

		return $this->cache[$name] = $template;
	}
}