<?php

namespace Application\DeskPRO\NewSearch\Filter;

interface FilterInterface
{
    public function getFilter();
} 