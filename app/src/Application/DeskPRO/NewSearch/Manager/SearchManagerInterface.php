<?php

namespace Application\DeskPRO\NewSearch\Manager;

/**
 * Search Manager Interface
 */
interface SearchManagerInterface
{
    public function quickSearch($q);
}