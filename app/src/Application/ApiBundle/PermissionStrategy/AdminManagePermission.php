<?php
/**************************************************************************\
| DeskPRO (r) has been developed by DeskPRO Ltd. http://www.deskpro.com/   |
| a British company located in London, England.                            |
|                                                                          |
| All source code and content Copyright (c) 2012, DeskPRO Ltd.             |
|                                                                          |
| The license agreement under which this software is released              |
| can be found at http://www.deskpro.com/license                           |
|                                                                          |
| By using this software, you acknowledge having read the license          |
| and agree to be bound thereby.                                           |
|                                                                          |
| Please note that DeskPRO is not free software. We release the full       |
| source code for our software because we trust our users to pay us for    |
| the huge investment in time and energy that has gone into both creating  |
| this software and supporting our customers. By providing the source code |
| we preserve our customers' ability to modify, audit and learn from our   |
| work. We have been developing DeskPRO since 2001, please help us make it |
| another decade.                                                          |
|                                                                          |
| Like the work you see? Think you could make it better? We are always     |
| looking for great developers to join us: http://www.deskpro.com/jobs/    |
|                                                                          |
| ~ Thanks, Everyone at Team DeskPRO                                       |
\**************************************************************************/

/**
 * DeskPRO
 *
 * @package DeskPRO
 * @category Entities
 */

namespace Application\ApiBundle\PermissionStrategy;

use Application\ApiBundle\ApiUser;
use Application\DeskPRO\Entity\ApiKey;

/**
 * The AdminManage permission is applied to APIs that:
 * - Have an admin user logged in (eg they have a session tied to their key)
 * - Their API key has a 'admin_manage' flag
 */
class AdminManagePermission implements PermissionStrategyInterface
{
	/**
	 * {@inheritDoc}
	 */
	public function userHasPermission(ApiUser $api_user, $context_info = null)
	{
		// The API key itself has the correct flag set
		if ($api_user->api_key) {
			if ($api_user->api_key->isFlagSet(ApiKey::FLAG_ADMIN_MANAGE)) {
				return true;
			}
		}

		// This is a logged-in session
		if ($api_user->session && $api_user->person && $api_user->person->can_admin) {
			return true;
		}

		return false;
	}
}